// CRUD Operations
/*
	C - Create (Insert document)
	R - Read/Retrieve (View specific document)
	U - Update (Edit specific document)
	D - Delete (Remove specific document)

	- CRUD Operations are the heart of any backend application.
*/

// [SECTION] Insert a documents (Create)

/*
	-Syntax:
		- db.collectionName.insertOne({object});
	Comparison with javascript
		object.object.method({object});
*/

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@gmail.com"
    },
    courses: ["CSS", "JavaScript", "Phython"],
    department: "none"
});


db.rooms.insertOne({
    name: "single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with simple necessities.",
    rooms_available: 10,
    isAvailable: true
});

//Insert Many
//Syntax -> db.collectionName.insertMany([objectA], [objectB]);

db.users.insertMany([{
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
        phone: "87654321",
        email: "S.Hawking@gmail.com"
    },
    courses: ["Phython", "React", "PHP"],
    department: "none"
}, {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
        phone: "87654321",
        email: "N.Armstrong@gmail.com"
    },
    courses: ["React", "Laravel", "Sass"],
    department: "none"
}]);


db.rooms.insertMany([{
    name: "double",
    accomodates: 3,
    price: 2000,
    description: "A room fit for a small family going on a vacation.",
    rooms_available: 5,
    isAvailable: false
}, {
    name: "queen",
    accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway.",
    rooms_available: 15,
    isAvailable: false
}]);

//Retrieve a document (read)
//Syntax -> db.collectionName.find({})
//Syntax -> db.collectionName.find({field:value})
//Syntax Multiple -> db.collectionName.find({fieldA:valueA, fieldB:valueB})

db.users.find({});

db.users.find({ firstName: "Stephen" });

db.users.find({ department: "none" });


db.users.find({ lastName: "Armstrong" }, { age: 82 });


//Updating Documents (Update)

//create a document to update -> sample data to update
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    course: [],
    department: "none"
});

/* Syntax -> db.collectionName.updateOne(
    {criteria}, 
    {$set: 
        {field: value}
    }); */

db.users.updateOne({ firstName: "Test" }, {
    $set: {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "b.gates@gmail.com"
        },
        course: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
    }
});

//Updating Multiple
//Syntax ->

db.users.updateMany({ department: "none" }, {
    $set: {
        department: "HR"
    }
});

//update thru ID
db.users.updateOne({ _id: ObjectId("63081ecbca17cb8b77798d0d") }, {
    $set: {
        firstName: "Romenick",
        lastName: "Garcia"
    }
});

//Replace One
//Syntax -> db.collectionName.replaceOne({criteria}, {field: value});

db.users.replaceOne({ firstName: "Bill" }, {
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact: {
        phone: "12345678",
        email: "b.gates@gmail.com"
    },
    courses: ["PHP", "Laravel", "HTML"],
    department: "Operations",
});

db.rooms.updateOne({ name: "queen" }, {
    $set: {
        "rooms_available": 0
    }
});

// DELETE -> removing documents
//Syntax -> db.collectionName.deleteOne({criteria});

//delete one
db.users.deleteOne({
    firstName: "Test"
});

//delete many
//Syntax -> db.collectionName.deleteMany({criteria});
//Syntax -> db.collectionName.deleteMany({}); -> do not use

db.users.deleteMany({
    firstName: "Test"
});

db.rooms.deleteOne({
    rooms_available: 0
});

//Advance Queries

//Query an Embeded Document
db.users.find({
    contact: {
        phone: "87654321",
        email: "S.Hawking@gmail.com"
    }
});

//dot notation
db.users.find({
    "contact.email": "S.Hawking@gmail.com"
});

//Exact Element
db.users.find({
    courses: ["CSS", "JavaScript", "Phython"]
});

//Disregard Element order
//$all -> matches documents where the field contains the nested array elements.

db.users.find({
    courses: { $all: ["JavaScript", "CSS", "Phython"] }
});

db.users.find({
    courses: { $all: ["Phython"] }
}); //retrieve all documents containing "Phython" as element

//Querrying an embedded array

db.users.insertOne({
    nameArr: [{
            nameA: "Juan"
        },
        {
            nameB: "Tamad"
        }
    ]
});

db.users.find({
    nameArr: {
        nameA: "Juan"
    }
});